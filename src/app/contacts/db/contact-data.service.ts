
import {Contact} from './contact';
import { InMemoryDbService } from 'angular-in-memory-web-api';

export class ContactData implements InMemoryDbService {
  createDb(){
    const contacts: Contact[] = [
        {
            id:1,
            firstName:"ramesh",
            lastName:"ramarjun",
            email:"ramesh@kk.com",
            phone:"1234567890",
            status:"active"
        },
        {
            id:2,
            firstName:"abc",
            lastName:"abcjun",
            email:"abc@kk.com",
            phone:"1234567890",
            status:"active"
        },
        {
            id:3,
            firstName:"def",
            lastName:"defjun",
            email:"def@kk.com",
            phone:"1234567890",
            status:"active"
        },
        {
            id:4,
            firstName:"ghij",
            lastName:"ghijjun",
            email:"ghij@kk.com",
            phone:"1234567890",
            status:"active"
        }
    ];

    return {contacts};
  }
}
