export class Contact {
    constructor( 
        public id = 0, 
        public firstName = '', 
        public lastName = '', 
        public email = '',
        public phone = '',
        public status = '') {}
}
