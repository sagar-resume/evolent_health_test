import { Component, OnInit } from '@angular/core';
import { Contact } from '../../db/contact';
import { CrudService } from '../../crud.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  contacts: Contact[] = [];

  constructor(public crudService: CrudService) { }

  ngOnInit() {
    this.getContacts();  
  }

  getContacts() {
    this.crudService.getContacts().subscribe((data: Contact[])=>{
      // console.log(data);
      this.contacts = data;
    })
  }

  deleteUser(id:number) {
    this.crudService.deleteUser(id).subscribe(data => {
       this.getContacts();
    });
  }
}
