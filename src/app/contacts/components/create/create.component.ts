import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../crud.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router ,ActivatedRoute} from '@angular/router';
import { error } from '@angular/compiler/src/util';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  contactForm: FormGroup;
  currentOp: string;
  submitted = false;
  selectedId:number;
  updatedObj:any;

  constructor(
    public fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public crudService: CrudService
  ){ }
  
  ngOnInit() {
    this.selectOp();

    this.contactForm = this.fb.group({
        firstName: ['',Validators.required],
        lastName: ['',Validators.required],
        phone: ['',[Validators.required,Validators.minLength(10),Validators.maxLength(12)]],
        email: ['',[Validators.required, Validators.email]]
    })
  }

  get CC() { return this.contactForm.controls; }

  submitForm() {
    this.submitted = true;

    if(this.contactForm.valid) {
      if(this.currentOp=='Create') {
        this.crudService.addContact(this.contactForm.value).subscribe(data => {
          // console.log('contact created!');
          this.router.navigate(['/contact/home/']);
        });
      }
      if(this.currentOp=='Update') {
        this.contactForm.value['id']=this.updatedObj['id'];
        this.contactForm.value['status']=this.updatedObj['status'];
        this.crudService.updateContact(this.contactForm.value).subscribe(data1 => {
          console.log('contact updated!');
          this.router.navigate(['/contact/home/']);
        });
      }
    }
    
  }

  selectOp() {
    let url:string=this.router.url;
    if(url.indexOf('update')>0) {
      this.currentOp='Update';

      this.selectedId=this.activatedRoute['params']['value']['contactId'];
      if(this.selectedId){
        this.crudService.getContact(this.selectedId).subscribe(data => {
          // console.log('data',data);
          this.updatedObj=data;
          this.contactForm.patchValue(data);
        }, error => console.log(error));
      }      
    } else {
      this.currentOp='Create';
    }

  }

  // getRecord(id:number) {
  //   this.crudService.getById(id).subscribe((data)=>{
  //     console.log(data);
  //   },error => console.log(error))
  // }
}
