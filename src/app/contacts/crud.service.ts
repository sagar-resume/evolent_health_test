import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import {  throwError, Observable, of } from 'rxjs';
import { catchError, filter, toArray,tap, map } from 'rxjs/operators';
import { Contact } from './db/contact';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  // private apiServer = "assets/jsons/contacts.json";
  private apiServer = "api/contacts";
  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  private handleError(error: any) {
    console.error(error);
    return throwError(error);
  }

  getContacts(): Observable<Contact[]> {
      return this.http.get<Contact[]>(this.apiServer).pipe(
        tap(data => data),
        catchError(this.handleError)
      );
  }

  getContact(id: number): Observable<Contact> {
    const url = `${this.apiServer}/${id}`;
    return this.http.get<Contact>(url).pipe(
      catchError(this.handleError)
    );
  }

  addContact(contact: Contact): Observable<Contact> {
    contact.id=null;
    return this.http.post<Contact>(this.apiServer, contact, this.httpOptions).pipe(
      tap(data =>{ data.status='active';}),
      catchError(this.handleError)
    );
  }

  deleteUser(id: number): Observable<Contact> {
    const url = `${this.apiServer}/${id}`;
    return this.http.delete<Contact>(url, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  updateContact(contact: Contact): Observable<Contact>{
    const url = `${this.apiServer}/${contact.id}`;
    return this.http.put<Contact>(url, contact, this.httpOptions).pipe(
      map(() => contact),
      catchError(this.handleError)
    );
  }

}
