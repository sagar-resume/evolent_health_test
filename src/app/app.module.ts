import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './contacts/components/home/home.component';
import { DetailsComponent } from './contacts/components/details/details.component';
import { CreateComponent } from './contacts/components/create/create.component';
import { UpdateComponent } from './contacts/components/update/update.component';
import { ContactData } from './contacts/db/contact-data.service';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DetailsComponent,
    CreateComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    InMemoryWebApiModule.forRoot(ContactData)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
