import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './contacts/components/home/home.component';
import { DetailsComponent } from './contacts/components/details/details.component';
import { UpdateComponent } from './contacts/components/update/update.component';
import { CreateComponent } from './contacts/components/create/create.component';

const routes: Routes = [
  { path: 'contact/home', component: HomeComponent },
  { path: 'contact/details/:contactId', component: DetailsComponent },
  { path: 'contact/update/:contactId', component: CreateComponent },
  { path: 'contact/create', component: CreateComponent},
  { path: 'contact', redirectTo: 'contact/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
